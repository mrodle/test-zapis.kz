//
//  ItemDetail.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import Foundation

// MARK: - ItemDetail
struct ItemDetail: Codable {
    let firm: Firm
    let masters: [Master]
    let services: [Service]
    let categories: [Category]
    let isOnline: Bool
    let location: Location
    let instagram: String?
}

// MARK: - Category
struct Category: Codable {
    let id: Int
    let name: String
}

// MARK: - Firm
struct Firm: Codable {
    let id: Int
    let name, address, type: String
    let avatarURL: String?
    let prepaymentCashbackAmount: String?
    let todayReservationsCount: Int?
    let phoneNumbers: [String]?
    let pictures: [String]?
    let averageRating: Double?
    let checkRating: Double?

    enum CodingKeys: String, CodingKey {
        case id, name, address, checkRating
        case type
        case avatarURL = "avatarUrl"
        case phoneNumbers, pictures, prepaymentCashbackAmount
        case averageRating, todayReservationsCount
    }
}

// MARK: - Location
struct Location: Codable {
    let type: String
    let markerX, markerY, centerX, centerY: Double
    let zoom: Int
}

// MARK: - Master
struct Master: Codable {
    let id: Int
    let name, surname: String
    let profession: String
    let avatarURL: String?
    let rating: Double?
    let experience: String?

    enum CodingKeys: String, CodingKey {
        case id, name, surname, profession
        case avatarURL = "avatarUrl"
        case rating, experience
    }
}

// MARK: - Service
struct Service: Codable {
    let id: Int
    let name: String
    let serviceDescription: String?
    let price, priceMax, duration: Int
    let express: Int?
    let priceStr: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case serviceDescription = "description"
        case price, priceMax, duration, express, priceStr
    }
}
