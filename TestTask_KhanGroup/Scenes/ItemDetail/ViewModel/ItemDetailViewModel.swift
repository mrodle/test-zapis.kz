//
//  ItemDetailViewModel.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
class ItemDetailViewModel: DefaultViewModelOutput{
    var error: Observable<String> = Observable("")
    var loading: Observable<Bool> = Observable(false)
    var item: Observable<ItemDetail?> = Observable(nil)
    let list: Observable<[DetailTableSection]> = Observable([])

    func getItemBy(id: Int) {
        self.loading.value = true
        ParseManager.shared.getRequest(url: AppConstants.API.itemDetail + id.description, success: { (result: GeneralResult<ItemDetail>) in
            self.loading.value = false
            self.list.value = self.populateList(with: result.data)
            self.item.value = result.data
        }) { (error) in
            self.loading.value = false
            self.error.value = error
        }
    }
    
    private func populateList(with model: ItemDetail) -> [DetailTableSection] {
        let recommendSection = DetailTableSection(type: .none, rowList: model.services)
        let mastersSection = DetailTableSection(type: .masters, rowList: model.masters)
        return [recommendSection, mastersSection]
    }

}
