//
//  DetailNavBar.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit


class DetailNavBar: UIView {

    var toBackClosure = {() -> () in }
    private var fromDetail: Bool
    
    private var backButton = UIButton()
    private var favouriteButton = UIButton()
    private var titleLabel = UILabel()
    
    init(fromDetail: Bool = true) {
        self.fromDetail = fromDetail
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeButtonImages(backButton: UIImage, favouriteButton: UIImage, hideTitle: Bool) {
        self.backButton.setImage(backButton, for: .normal)
        self.favouriteButton.setImage(favouriteButton, for: .normal)
        self.titleLabel.isHidden = hideTitle
    }
    
    func configureTitle(_ title: String) {
        titleLabel.text = title
    }
    
    @objc private func backAction() {
        toBackClosure()
    }
}

extension DetailNavBar {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
        setupActions()
    }
    
    private func addSubviews() {
        addSubview(backButton)
        addSubview(favouriteButton)
        addSubview(titleLabel)
    }
    
    private func addConstraints() {
        backButton.snp.makeConstraints { (make) in
            make.left.equalTo(24)
            make.bottom.equalTo(-((AppConstants.navBarHeight/2)-11))
            make.height.width.equalTo(22)
        }
        
        favouriteButton.snp.makeConstraints { (make) in
            make.right.equalTo(-24)
            make.centerY.equalTo(backButton)
            make.height.width.equalTo(25)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(backButton.snp.right).offset(4)
            make.right.equalTo(favouriteButton.snp.left).offset(-4)
            make.centerY.equalTo(backButton)
        }


    }
    
    private func stylizeViews() {
        backgroundColor = fromDetail ? .clear : .white
        titleLabel.textColor = .black
        titleLabel.font = .getSFProSemiboldFont(ofSize: 17)
        titleLabel.textAlignment = .center
        titleLabel.isHidden = true

        backButton.setImage(fromDetail ? #imageLiteral(resourceName: "arrow-3") : #imageLiteral(resourceName: "arrow-4"), for: .normal)
        favouriteButton.setImage(fromDetail ? #imageLiteral(resourceName: "heart") : nil, for: .normal)
        
    }
    
    private func setupActions() {
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
}
