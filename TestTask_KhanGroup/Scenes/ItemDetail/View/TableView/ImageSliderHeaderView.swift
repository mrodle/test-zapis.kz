//
//  ImageSliderHeaderView.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImageSliderHeaderView: UIView {

//    MARK: - Properties
    private var sliders = [UIImage]()
    var presentBlock = {(_ controller: UIViewController) -> () in}
    private var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate? = nil


    private var layout = UICollectionViewFlowLayout()
    private var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private var pageControl = UIPageControl()
    
//    MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupData(_ pictureList: [String]?) -> Void {
        self.sliders = []
        if let list = pictureList {
            list.forEach({
                downloadImage(with: $0.serverUrlString.url, completion: {
                    self.pageControl.isHidden = self.sliders.count <= 1
                    self.pageControl.numberOfPages = self.sliders.count
                    self.collectionView.reloadData()
                })
            })
        }
    }

    func downloadImage(`with` urlString : URL, completion: @escaping (() -> ())){
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: urlString) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        guard let `self` = self else { return }
                        self.sliders.append(image)
                        completion()
                    }
                }
            }
        }
    }

    private func presentToFullScreen(cell: SliderCollectionViewCell, indexPath: IndexPath) {
        let fullScreenController = FullScreenSlideshowViewController()
        fullScreenController.inputs = sliders.map { ImageSource(image: $0) }
        fullScreenController.initialPage = indexPath.row
        
        slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(imageView: cell.imageView, slideshowController: fullScreenController)
        fullScreenController.transitioningDelegate = slideshowTransitioningDelegate
        
        fullScreenController.slideshow.currentPageChanged = { [weak self] page in
            self?.slideshowTransitioningDelegate?.referenceImageView = cell.imageView
        }
        if cell.imageView.image != #imageLiteral(resourceName: "no_image") {
            presentBlock(fullScreenController)
        }
    }
    
    private func scrollConfiguration(indexItem: Int) {
        pageControl.currentPage = indexItem
    }
}

extension ImageSliderHeaderView {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(collectionView)
        addSubview(pageControl)
    }
    
    private func addConstraints() {
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.height.equalTo(AppConstants.screenWidth*0.8)
        }
        
        pageControl.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom).offset(16)
            make.width.bottom.centerX.equalToSuperview()
            make.height.equalTo(25)
        }
    }
    
    private func stylizeViews() {

        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .white
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SliderCollectionViewCell.self, forCellWithReuseIdentifier: SliderCollectionViewCell.cellIdentifier())
        
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.9058823529, alpha: 1)
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.numberOfPages = 5
        pageControl.isUserInteractionEnabled = false


    }
}

extension ImageSliderHeaderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCollectionViewCell.cellIdentifier(), for: indexPath) as! SliderCollectionViewCell
        
        cell.configure(image: sliders[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: frame.width, height: frame.width*0.8)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? SliderCollectionViewCell{
            presentToFullScreen(cell: cell, indexPath: indexPath)
        }

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else {return}
        scrollConfiguration(indexItem: indexPath.item)
    }
}
