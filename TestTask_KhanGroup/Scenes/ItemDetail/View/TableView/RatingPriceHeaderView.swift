//
//  RatingPriveHeaderView.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class RatingPriceHeaderView: UIView {

    //    MARK: - Properties
    
    private var checkRatingView = ItemRatingInfoView(type: .check)
    private var countRatingLabel = UILabel()
    private var priceLabel = UILabel()
    private var ratingView = ItemRatingInfoView(type: .rating)
    private var cashbackLabel = UILabel()
    
    //    MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    MARK: - Setup functions
    func setupData(item: ItemDetail) {
        ratingView.configureValue(value: (item.firm.averageRating ?? 0.0).description)
        checkRatingView.configureValue(value: (item.firm.checkRating ?? 0).description)
        priceLabel.text = "     ₸ ₸ ₸"
        if let cashback = item.firm.prepaymentCashbackAmount {
            cashbackLabel.isHidden = false
            cashbackLabel.text = "Кэшбек " + cashback
        } else {
            cashbackLabel.isHidden = true
        }
    }
}

extension RatingPriceHeaderView {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(ratingView)
        addSubview(countRatingLabel)
        addSubview(checkRatingView)
        addSubview(priceLabel)
        addSubview(cashbackLabel)
    }
    
    private func addConstraints() {
        ratingView.snp.makeConstraints { (make) in
            make.right.equalTo(-16)
            make.top.equalTo(8)
        }
        countRatingLabel.snp.makeConstraints { (make) in
            make.top.equalTo(ratingView.snp.bottom).offset(4)
            make.right.equalTo(ratingView)
        }
        
        checkRatingView.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.centerY.equalTo(ratingView.snp.bottom)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(checkRatingView.snp.right).offset(4)
            make.centerY.equalTo(checkRatingView)
        }
        
        cashbackLabel.snp.makeConstraints { (make) in
            make.top.equalTo(countRatingLabel.snp.bottom).offset(16)
            make.right.left.bottom.equalToSuperview()
            make.height.equalTo(35)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
                
        checkRatingView.configureValue(value: "1")
        ratingView.configureValue(value: "3.3")
        
        countRatingLabel.textColor = .lightGray
        countRatingLabel.textAlignment = .center
        countRatingLabel.font = .getMontserratSemiBoldFont(ofSize: 12)
        countRatingLabel.text = "54 reviews"

        priceLabel.textColor = .lightGray
        priceLabel.textAlignment = .center
        priceLabel.font = .getMontserratSemiBoldFont(ofSize: 12)
        priceLabel.text = "TTT"
                
        cashbackLabel.backgroundColor = #colorLiteral(red: 255.0/255.0, green: 196.0/255.0, blue: 0.0, alpha: 1.0)
        cashbackLabel.textColor = .white
        cashbackLabel.textAlignment = .center
        cashbackLabel.font = .getMontserratMediumFont(ofSize: 16)
        cashbackLabel.text = "do 20% cashback from price"
    }
}
