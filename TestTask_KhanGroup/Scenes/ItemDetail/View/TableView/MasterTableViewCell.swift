//
//  MasterTableViewCell.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class MasterTableViewCell: DefaulDetailTableViewCell {
    private var profileView = UIImageView()
    private var titleLabel = UILabel()
    private var subTitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure(model: Codable) {
        if let model = model as? Master {
            titleLabel.text = model.name
            subTitleLabel.text = model.profession
            if let url = model.avatarURL {
                profileView.load(url: url.serverUrlString.url)
            }
        }
    }
}

extension MasterTableViewCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubview(profileView)
        addSubview(titleLabel)
        addSubview(subTitleLabel)
    }
    
    private func addConstraints() {
        profileView.snp.makeConstraints { (make) in
            make.top.left.equalTo(12)
            make.bottom.equalTo(-12)
            make.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.right.equalTo(-12)
            make.left.equalTo(profileView.snp.right).offset(8)
        }
        subTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(profileView.snp.right).offset(8)
            make.right.equalTo(-12)
        }
    }
    
    private func stylizeViews() {
        accessoryType = .disclosureIndicator
        backgroundColor = .white
        
        profileView.layer.cornerRadius = 20
        profileView.layer.masksToBounds = true
        
        titleLabel.font = .systemFont(ofSize: 15)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0

        subTitleLabel.font = .systemFont(ofSize: 12)
        subTitleLabel.textColor = .gray
        subTitleLabel.numberOfLines = 0
    }
}
