//
//  CategoriesDetailHeaderView.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class CategoriesDetailHeaderView: UIView {
    
    
    private var itemList: [String] = []{
        didSet {
            collectionView.reloadData()
        }
    }
    
    private var layout = UICollectionViewFlowLayout()
    private var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    //    MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupData(itemList: [String]) {
        self.itemList = itemList
    }
}

extension CategoriesDetailHeaderView {
    private func setupViews() {
        addSubviews()
        setupConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(collectionView)
    }
    
    private func setupConstraints() {

        collectionView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(12)
            make.bottom.equalTo(-20)
            make.height.equalTo(64)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .clear

        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 12
        layout.minimumInteritemSpacing = 0
        
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(DetailCategoriesCollectionViewCell.self, forCellWithReuseIdentifier: DetailCategoriesCollectionViewCell.cellIdentifier())
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)

    }
}

extension CategoriesDetailHeaderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailCategoriesCollectionViewCell.cellIdentifier(), for: indexPath) as! DetailCategoriesCollectionViewCell
        
        cell.configure(itemList[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = "\(itemList[indexPath.row])"
        let cellWidth = text.size(withAttributes:[.font: UIFont.getSFProSemiboldFont(ofSize: 18)]).width + 30.0

        return CGSize(width: cellWidth, height: 64)
    }
    
}
