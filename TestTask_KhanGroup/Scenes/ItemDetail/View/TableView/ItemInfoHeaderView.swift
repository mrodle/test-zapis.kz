//
//  ItemInfoHeaderView.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

protocol DetailContactButtonDelegate {
    func showContactButtonAlert(type: DetailInfoButtonType, alert: UIAlertController)
    func tapAlertButtonDelegate(type: DetailInfoButtonType, values: String)
}

class ItemInfoHeaderView: UIView {
    
    var delegate: DetailContactButtonDelegate?

    private var instagramAccountList = [String]()
    private var phoneNumberList = [String]()
    
    private var titleLabel = UILabel()
    private var typeLabel = UILabel()
    private var locationButton = DetailInfoButton(type: .location, title: "ул Абылайхана 53 Абылайхан плаза ханаа")
    private var instagramButton = DetailInfoButton(type: .instagram)
    private var phoneButton = DetailInfoButton(type: .phone)
    private var separatorView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupData(item: ItemDetail) {
        self.phoneNumberList = item.firm.phoneNumbers!
        self.titleLabel.text = item.firm.name
        self.typeLabel.text = item.firm.type
        self.locationButton.setTitle(item.firm.address, for: .normal)
        if let instagram = item.instagram {
            self.instagramAccountList.append(instagram)
        }
    }
    
    @objc private func detailButtonAction(_ sender: DetailInfoButton) {
        let contactAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        switch sender.type {
        case .location:
            break
        case .instagram:
            for account in instagramAccountList {
                let accountAction = UIAlertAction(title: "@\(account)", style: .default) { (action: UIAlertAction) in
                // Code to unfollow
                    self.delegate?.tapAlertButtonDelegate(type: .instagram, values: "https://instagram.com/\(account)")
                }
                contactAlert.addAction(accountAction)
            }
        case .phone:
            for phoneNumber in phoneNumberList {
                let phoneNumberAction = UIAlertAction(title: phoneNumber, style: .default) { (action: UIAlertAction) in
                // Code to unfollow
                    self.delegate?.tapAlertButtonDelegate(type: .phone, values: phoneNumber)
                }
                contactAlert.addAction(phoneNumberAction)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        contactAlert.addAction(cancelAction)
        
        delegate?.showContactButtonAlert(type: sender.type, alert: contactAlert)
        
    }
}

extension ItemInfoHeaderView {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
        setupActions()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(typeLabel)
        addSubview(phoneButton)
        addSubview(instagramButton)
        addSubview(locationButton)
        addSubview(separatorView)
    }

    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.equalTo(16)
            make.right.equalTo(-16)
        }
        typeLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom).offset(12)
        }
        
        phoneButton.snp.makeConstraints { (make) in
            make.right.equalTo(titleLabel)
            make.top.equalTo(typeLabel.snp.bottom).offset(12)
            make.height.width.equalTo(50)
        }
        
        instagramButton.snp.makeConstraints { (make) in
            make.right.equalTo(phoneButton.snp.left).offset(-12)
            make.top.height.width.equalTo(phoneButton)
        }
        
        locationButton.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel)
            make.right.equalTo(instagramButton.snp.left).offset(-12)
            make.top.height.equalTo(phoneButton)
        }
        
        separatorView.snp.makeConstraints { (make) in
            make.left.right.equalTo(titleLabel)
            make.top.equalTo(instagramButton.snp.bottom).offset(12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }


    }

    private func stylizeViews() {
        titleLabel.textColor = .black
        titleLabel.font = .getSFProMediumFont(ofSize: 18)
        titleLabel.text = "Chocolate"
        
        typeLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        typeLabel.font = .getSFProMediumFont(ofSize: 16)
        typeLabel.text = "chocolate"
        
        separatorView.backgroundColor = .lightGray
    }
    
    private func setupActions() {
        locationButton.addTarget(self, action: #selector(detailButtonAction(_:)), for: .touchUpInside)
        instagramButton.addTarget(self, action: #selector(detailButtonAction(_:)), for: .touchUpInside)
        phoneButton.addTarget(self, action: #selector(detailButtonAction(_:)), for: .touchUpInside)
    }
}
