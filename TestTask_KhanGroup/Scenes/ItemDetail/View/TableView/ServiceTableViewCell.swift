//
//  ServiceTableViewCell.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class ServiceTableViewCell: DefaulDetailTableViewCell {

    private var titleLabel = UILabel()
    private var subTitleLabel = UILabel()
    private var addButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure(model: Codable) {
        if let model = model as? Service {
            titleLabel.text = model.name
            subTitleLabel.text = "\(model.duration) мин * \(model.priceMax) - \(model.price)"
        }
    }
}

extension ServiceTableViewCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(addButton)
    }
    
    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(16)
            make.right.equalTo(-60)
        }
        subTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(16)
            make.bottom.equalTo(-12)
        }
        addButton.snp.makeConstraints { (make) in
            make.right.equalTo(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
        
        titleLabel.font = .systemFont(ofSize: 15)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        
        subTitleLabel.font = .systemFont(ofSize: 12)
        subTitleLabel.textColor = .gray
        
        if #available(iOS 13.0, *) {
            addButton.setImage(UIImage(systemName: "plus.square"), for: .normal)
            addButton.tintColor = .black
        } else {
            // Fallback on earlier versions
        }
    }
}
