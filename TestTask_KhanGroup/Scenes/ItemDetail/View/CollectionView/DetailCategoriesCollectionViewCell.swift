//
//  DetailCategoriesCollectionViewCell.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class DetailCategoriesCollectionViewCell: UICollectionViewCell {

    private var backView = UIView()
    private var titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(_ category: String) {
        titleLabel.text = category
    }
}
extension DetailCategoriesCollectionViewCell {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(backView)
        backView.addSubview(titleLabel)
    }
    
    private func addConstraints() {
        backView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalTo(-12)
            make.top.equalTo(8)
            make.bottom.equalTo(-16)
        }
        
        
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    
    private func stylizeViews() {
        backgroundColor = .clear
        
        backView.backgroundColor = #colorLiteral(red: 0.7513522581, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5)
        backView.layer.cornerRadius = 20

        backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08).cgColor
        backView.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        backView.layer.shadowOpacity = 1.0
        backView.layer.shadowRadius = 16.0


        titleLabel.textColor = .gray
        titleLabel.textAlignment = .center
        titleLabel.font = .getSFProSemiboldFont(ofSize: 16)
        
    }
    
    
}
