//
//  DetailInfoButton.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit



class DetailInfoButton: UIButton {

    var type: DetailInfoButtonType
    init(type: DetailInfoButtonType, title: String? = nil) {
        self.type = type
        super.init(frame: .zero)
        self.imageEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        self.setTitle(title, for: .normal)
        self.setTitleColor(.lightGray, for: .normal)
        self.titleLabel?.font = .getSFProMediumFont(ofSize: 14)
        self.titleLabel?.numberOfLines = 2
        self.imageView?.contentMode = .scaleAspectFit
        self.backgroundColor = #colorLiteral(red: 0.7513522581, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5)
        self.layer.cornerRadius = 20
        switch type {
        case .location:
            self.setImage(#imageLiteral(resourceName: "pin"), for: .normal)
        case .instagram:
            self.setImage(#imageLiteral(resourceName: "instagram-sketched"), for: .normal)
        case .phone:
            self.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
