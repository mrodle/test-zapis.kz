//
//  MapViewController.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit
import YandexMapKit

class MapViewController: UIViewController {
    
    private var location: Location
    private var mapView = YMKMapView()
    private var collection: YMKClusterizedPlacemarkCollection?
    private var navbar = DetailNavBar(fromDetail: false)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        configure(location: location)
    }
    
    init(location: Location) {
        self.location = location
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure(location: Location) {
        collection = mapView.mapWindow.map.mapObjects.addClusterizedPlacemarkCollection(with: self)

        let center = YMKPoint(latitude: Double(location.centerY), longitude: Double(location.centerX))
        mapView.mapWindow.map.move(
            with: YMKCameraPosition(target: center, zoom: Float(location.zoom), azimuth: 0, tilt: 0),
            animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.3),
            cameraCallback: nil)
        
        let point = YMKPoint(latitude: Double(location.markerY), longitude: Double(location.markerX))
        let placemark = collection?.addPlacemark(with: point,
                                                 image: #imageLiteral(resourceName: "marker-2"),
                                                 style: YMKIconStyle.init())
        placemark?.userData = "user data"
        
        collection?.clusterPlacemarks(withClusterRadius: 60, minZoom: 15)
        
    }
}
extension MapViewController: YMKClusterListener {
    func onClusterAdded(with cluster: YMKCluster) { }
}

// MARK: - Stylize
extension MapViewController {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
        setupActions()
    }
    
    private func addSubviews() {
        view.addSubview(mapView)
        view.addSubview(navbar)
    }
    
    private func addConstraints() {
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        navbar.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(AppConstants.statusBarHeight + AppConstants.navBarHeight)
        }

    }
    
    private func stylizeViews() {
        view.backgroundColor = .white
    }
    
    private func setupActions() {
        navbar.toBackClosure = {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
