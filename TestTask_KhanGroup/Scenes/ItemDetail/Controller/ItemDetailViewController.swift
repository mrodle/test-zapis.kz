//
//  ItemDetailViewController.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController {

    private var id: Int
    private var viewModel = ItemDetailViewModel()
    
    private var navbar = DetailNavBar()
    private var tableView = UITableView()
    private var tableHeaderView = ImageSliderHeaderView()
    private var infoHeaderView = ItemInfoHeaderView()
    private var ratingPriceHeaderView = RatingPriceHeaderView()
    private var categoryHeaderView = CategoriesDetailHeaderView()
    
    init(id: Int) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(from: viewModel)
        viewModel.getItemBy(id: id)
    }
    
    private func bind(from viewModel: ItemDetailViewModel) {
        viewModel.error.observe(on: self) { [weak self] in
            guard  let `self` = self else { return }
            self.showAlert(type: .error, $0)}
        viewModel.loading.observe(on: self) { loading in
            if (loading) {
                LoaderView.show()
            } else {
                self.tableView.isHidden = false
                self.tableView.refreshControl?.endRefreshing()
                LoaderView.hide()
            }
        }
        viewModel.item.observe(on: self) { [weak self] in
            if $0 != nil {
                guard  let `self` = self else { return }
                self.setupData()
                self.tableView.reloadData()
            }
        }
    }
    
    private func setupData() {
        if let item = viewModel.item.value {
            navbar.configureTitle(item.firm.name)
            tableHeaderView.setupData(item.firm.pictures)
            infoHeaderView.setupData(item: item)
            ratingPriceHeaderView.setupData(item: item)
            categoryHeaderView.setupData(itemList: item.categories.map({$0.name}))
        }
    }
    
}

extension ItemDetailViewController {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
        setupActions()
    }
    
    private func addSubviews() {
        view.addSubview(tableView)
        view.addSubview(navbar)
    }
    
    private func addConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        navbar.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(AppConstants.statusBarHeight + AppConstants.navBarHeight)
        }

    }

    private func stylizeViews() {
        view.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
        tableView.tableHeaderView = tableHeaderView
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: AppConstants.screenWidth, height: AppConstants.screenWidth*0.8)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellIdentifier())
        tableView.register(MasterTableViewCell.self, forCellReuseIdentifier: MasterTableViewCell.cellIdentifier())
        tableView.register(ServiceTableViewCell.self, forCellReuseIdentifier: ServiceTableViewCell.cellIdentifier())
        
        infoHeaderView.delegate = self
    }
    
    func setupActions() {
        tableHeaderView.presentBlock = { [weak self] controller in
            guard let `self` = self else { return }
            self.present(controller, animated: true, completion: nil)
        }
        
        navbar.toBackClosure = {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ItemDetailViewController: DetailContactButtonDelegate {
    func showContactButtonAlert(type: DetailInfoButtonType, alert: UIAlertController) {
        if type == .location {
            self.navigationController?.pushViewController(MapViewController(location: viewModel.item.value!.location), animated: true)
        } else {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tapAlertButtonDelegate(type: DetailInfoButtonType, values: String) {
        switch type {
        case .location:
            break
        case .instagram:
            if let url = URL(string: values) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.open(url)
                }
            }
        case .phone:
            if let url = URL(string: "tel://\(values)") {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
}

extension ItemDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.list.value.count + 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 3, 4:
            return viewModel.list.value[section - 3].rowList.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0, 1, 2:
            let cell = UITableViewCell()

            return cell
        default:
            let sectionModel = viewModel.list.value[indexPath.section - 3]

            let cell = indexPath.section == 3 ? ServiceTableViewCell() : MasterTableViewCell()
            cell.configure(model: sectionModel.rowList[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return infoHeaderView
        case 1:
            return ratingPriceHeaderView
        case 2:
            return categoryHeaderView
        case 3, 4:
            let label = UILabel(frame: CGRect(x: 16, y: 0, width: AppConstants.screenWidth, height: 60))
            label.backgroundColor = .white
            label.font = .getSFProSemiboldFont(ofSize: 20)
            label.text = ["   Сервисы", "   Мастера"][section - 3]
            
            return label
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let navbarPresentingPoint = AppConstants.screenWidth*0.8 - AppConstants.navBarHeight - AppConstants.statusBarHeight

        navbar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: scrollView.contentOffset.y/navbarPresentingPoint)

        if scrollView.contentOffset.y/AppConstants.screenWidth*0.8 >= 0.4 {
            navbar.changeButtonImages(backButton: #imageLiteral(resourceName: "arrow-4"), favouriteButton: #imageLiteral(resourceName: "heart-2"), hideTitle: scrollView.contentOffset.y < 260)
        } else {
            navbar.changeButtonImages(backButton: #imageLiteral(resourceName: "arrow-3"), favouriteButton: #imageLiteral(resourceName: "heart"), hideTitle: scrollView.contentOffset.y < 260)
        }
    }

}
