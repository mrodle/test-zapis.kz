//
//  ItemListBySection.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation

enum ItemSectionType: String {
    case recommendedFirms   = "Рекомендуемое"
    case popularFirms       = "Популярные"
    case recentlyAddedFirms = "Новые"
    case masters            = "Мастера"
    case none               = "Неизвестная секция"
}

struct ItemListBySection {
    var type: ItemSectionType
    var value: [Item]
}

struct DetailTableSection {
    let type: ItemSectionType
    let rowList: [Codable]
}
