//
//  ItemListByType.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation

enum ItemSection: String {
    case recommendedFirms   = "Рекомендуемое"
    case popularFirms       = "Популярные"
    case recentlyAddedFirms = "Новые"
    case masters            = "Мастера"
    case none               = "Неизвестная секция"
}

struct ItemListBySection {
    var type: ItemSection
    var value: [Item]
}
