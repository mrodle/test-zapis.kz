//
//  Item.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation

struct Item: Codable {
    var id: Int
    var name: String
    var address: String
    var type: String
    var checkRating: Int
    var urlKey: String
    var isPromoted: Bool
    var avatarUrl: String?
    var isIndividualMaster: Bool
    var workSchedule: String
    var pictureUrl: String?
    var averageRating: Double
    var prepaymentCashbackAmount: String?
    var todayReservationsCount: Int?
    var isFavorite: Bool
}
