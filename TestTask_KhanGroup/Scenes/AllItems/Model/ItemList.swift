//
//  ItemList.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
struct ItemList: Codable {
    var list: [String: [Item]]
}

struct ItemSectionList: Codable {
    var updates: [Item]
    var recommendedFirms: [Item]
    var popularFirms: [Item]
    var recentlyAddedFirms: [Item]
    var masters: [Item]
    var cities: [City]
    var wrongServicePricePhones: [String]
}
