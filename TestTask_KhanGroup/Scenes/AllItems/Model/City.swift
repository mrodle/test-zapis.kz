//
//  City.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
struct City: Codable {
    var id: Int
    var name: String
    var urlName: String
    var longitude: Double
    var latitude: Double
}
