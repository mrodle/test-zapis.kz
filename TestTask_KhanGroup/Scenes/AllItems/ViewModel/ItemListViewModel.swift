//
//  ItemListViewModel.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
protocol DefaultViewModelOutput {
    var error: Observable<String> { get }
    var loading: Observable<Bool> { get }
}


final class ItemListViewModel: DefaultViewModelOutput {
    var error: Observable<String> = Observable("")
    var loading: Observable<Bool> = Observable(false)
    var list: Observable<[ItemListBySection]> = Observable([])
    var cityList: Observable<[City]> = Observable([])
    
    func getItemList() {
        self.list.value = []
        self.loading.value = true
        ParseManager.shared.getRequest(url: AppConstants.API.itemList, success: { (result: GeneralResult<ItemSectionList>) in
            self.loading.value = false
            self.list.value = self.getListBySection(from: result.data)
            self.cityList.value = result.data.cities
        }) { (error) in
            self.loading.value = false
            self.error.value = error
        }
    }
    
    private func getListBySection(from sections: ItemSectionList) -> [ItemListBySection] {
        var itemSectionList = [ItemListBySection]()
        itemSectionList.append(ItemListBySection(type: .recommendedFirms, value: sections.recommendedFirms))
        itemSectionList.append(ItemListBySection(type: .popularFirms, value: sections.popularFirms))
        itemSectionList.append(ItemListBySection(type: .recentlyAddedFirms, value: sections.recentlyAddedFirms))
        itemSectionList.append(ItemListBySection(type: .masters, value: sections.masters))

        return itemSectionList
    }
}
