//
//  CityFilterView.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit
class CityFilterView: UIView {
    
    private var titleLabel = UILabel()
    private var choiseButton = UIButton()
    var getCityListClosure = {() -> () in}

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setButtonAttributeString(_ string: String) {
        let attrString = NSMutableAttributedString(string: string,
        attributes: [
            NSAttributedString.Key.font: UIFont.getMontserratSemiBoldFont(ofSize: 18),
            NSAttributedString.Key.foregroundColor:  #colorLiteral(red: 0.863, green: 0.176, blue: 0.137, alpha: 1),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.underlineColor: #colorLiteral(red: 0.863, green: 0.176, blue: 0.137, alpha: 1),
            ])
        
        choiseButton.setAttributedTitle(attrString, for: .normal)
    }
    
    @objc func choiseButtonAction(_ sender: UIButton) {
        getCityListClosure()
    }

}

extension CityFilterView {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
        setupActions()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(choiseButton)
    }
    
    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.bottom.equalToSuperview()
        }
        
        choiseButton.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel.snp.right)
            make.top.bottom.equalTo(titleLabel)
            make.right.lessThanOrEqualTo(-16)
        }
    }
    
    private func stylizeViews() {
        titleLabel.font = .getSFProSemiboldFont(ofSize: 22)
        titleLabel.text = "Запись в "
        setButtonAttributeString("Алматы")
    }
    
    private func setupActions() {
        choiseButton.addTarget(self, action: #selector(choiseButtonAction(_:)), for: .touchUpInside)
    }
}
