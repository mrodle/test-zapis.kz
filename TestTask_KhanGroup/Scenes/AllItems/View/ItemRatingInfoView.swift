//
//  ItemRatingInfoView.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

class ItemRatingInfoView: UIView {
    
    private var type: ItemRatingInfoType = .check
    
    private var imageView = UIImageView()
    private var countLabel = UILabel()
    
    init(type: ItemRatingInfoType) {
        self.type = type
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureValue(value: String) -> Void {
        self.isHidden = type == .check && value == "0"
        countLabel.text = value
    }
}

extension ItemRatingInfoView {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(imageView)
        addSubview(countLabel)
    }
    
    private func addConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.right.top.bottom.equalToSuperview()
            make.height.width.equalTo(15)
        }
        countLabel.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(imageView.snp.left).offset(-4)
        }
    }
    
    private func stylizeViews() {
        imageView.contentMode = .scaleAspectFill
        imageView.image = type == .check ? #imageLiteral(resourceName: "two-2") : #imageLiteral(resourceName: "star")
        
        countLabel.textColor = type == .check ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) : #colorLiteral(red: 255.0/255.0, green: 196.0/255.0, blue: 0.0, alpha: 1.0)
        countLabel.font = .getMontserratSemiBoldFont(ofSize: 12)
        countLabel.textAlignment = type == .check ? .left : .right
        
    }
}
