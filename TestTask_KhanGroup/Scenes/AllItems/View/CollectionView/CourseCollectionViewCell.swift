//
//  CourseCollectionViewCell.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

class CourseCollectionViewCell: UICollectionViewCell {
    
    //    MARK: - Properties
    
    private var backView = UIView()
    private var imageView = UIImageView(image: #imageLiteral(resourceName: "no_image"))
    private var typeLabel = UILabel()
    private var titleLabel = UILabel()
    private var addressLabel = UILabel()
    private var separatorView = UIView()
    private var checkRatingView = ItemRatingInfoView(type: .check)
    private var priceLabel = UILabel()
    private var ratingView = ItemRatingInfoView(type: .rating)
    private var cashbackLabel = UILabel()
    
    //    MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
        setupAction()
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    MARK: - Setup functions
    
    private func setupAction() -> Void {
    }
    
    func configure(_ item: Item) -> Void {
        if let url = item.pictureUrl {
            self.imageView.load(url: url.serverUrlString.url)
        }
        
        typeLabel.text = item.type
        titleLabel.text = item.name
        addressLabel.text = item.address
        ratingView.configureValue(value: item.averageRating.description)
        checkRatingView.configureValue(value: item.checkRating.description)
        priceLabel.text = "  ₸ ₸ ₸"
        if let cashback = item.prepaymentCashbackAmount {
            cashbackLabel.isHidden = false
            cashbackLabel.text = "Кэшбек " + cashback
        } else {
            cashbackLabel.isHidden = true
        }
        
    }
    
    
}

extension CourseCollectionViewCell {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(backView)
        backView.addSubview(imageView)
        backView.addSubview(typeLabel)
        backView.addSubview(titleLabel)
        backView.addSubview(addressLabel)
        backView.addSubview(separatorView)
        backView.addSubview(checkRatingView)
        backView.addSubview(priceLabel)
        backView.addSubview(ratingView)
        backView.addSubview(cashbackLabel)
    }
    
    private func addConstraints() {
        backView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(8)
            make.bottom.equalTo(-19)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.height.equalTo(backView.snp.height).dividedBy(2)
            make.top.left.right.equalToSuperview()
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(8)
            make.left.right.equalToSuperview().inset(16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(typeLabel.snp.bottom).offset(2)
            make.left.right.equalTo(typeLabel)
        }
        
        addressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.left.right.equalTo(typeLabel)
        }
        
        separatorView.snp.makeConstraints { (make) in
            make.left.right.equalTo(typeLabel)
            make.top.equalTo(addressLabel.snp.bottom).offset(6)
            make.height.equalTo(0.5)
        }
        checkRatingView.snp.makeConstraints { (make) in
            make.left.equalTo(typeLabel)
            make.top.equalTo(separatorView.snp.bottom).offset(6)
            make.bottom.lessThanOrEqualTo(-4)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(checkRatingView.snp.right).offset(4)
            make.top.bottom.equalTo(checkRatingView)
        }
        
        ratingView.snp.makeConstraints { (make) in
            make.right.equalTo(typeLabel)
            make.left.greaterThanOrEqualTo(priceLabel.snp.right).offset(4)
            make.top.bottom.equalTo(checkRatingView)
        }
        
        cashbackLabel.snp.makeConstraints { (make) in
            make.right.equalTo(-16)
            make.bottom.equalTo(imageView).offset(-16)
            make.height.equalTo(20)
            make.left.equalTo(imageView.snp.centerX)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .clear
        
        backView.backgroundColor = .white
        backView.layer.cornerRadius = 20

        backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08).cgColor
        backView.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        backView.layer.shadowOpacity = 1.0
        backView.layer.shadowRadius = 16.0

        imageView.contentMode = .scaleAspectFill
        imageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true

        typeLabel.textColor = .lightGray
        typeLabel.font = .getSFProSemiboldFont(ofSize: 11)

        titleLabel.textColor = .black
        titleLabel.font = .getSFProSemiboldFont(ofSize: 12)
        
        addressLabel.textColor = #colorLiteral(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)
        addressLabel.font = .getSFProRegularFont(ofSize: 12)
        
        separatorView.backgroundColor = .lightGray
        
        checkRatingView.configureValue(value: "1")
        ratingView.configureValue(value: "3.3")
        
        priceLabel.textColor = .lightGray
        priceLabel.textAlignment = .center
        priceLabel.font = .getMontserratSemiBoldFont(ofSize: 12)
                
        cashbackLabel.backgroundColor = #colorLiteral(red: 255.0/255.0, green: 196.0/255.0, blue: 0.0, alpha: 1.0)
        cashbackLabel.textColor = .white
        cashbackLabel.textAlignment = .center
        cashbackLabel.font = .getMontserratMediumFont(ofSize: 10)
    }
    
    
}
