//
//  HorizontalItemListView.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class HorizontalItemListCell: UITableViewCell {
    
    var selectedClosure = {(_ item: Item) -> () in }
    
    private var itemList: [Item] = []{
        didSet {
            collectionView.reloadData()
        }
    }
    
    private var titleLabel = UILabel()
    private var layout = UICollectionViewFlowLayout()
    private var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    //    MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureValue(title: ItemSectionType, itemList: [Item]) {
        self.titleLabel.text = title.rawValue
        self.itemList = itemList
    }
}

extension HorizontalItemListCell {
    private func setupViews() {
        addSubviews()
        setupConstraints()
        stylizeViews()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(collectionView)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(24)
            make.top.equalTo(4)
            make.right.equalTo(-4)
        }

        collectionView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(12)
            make.height.equalTo(220)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        titleLabel.textColor = .textColor
        titleLabel.font = .getSFProSemiboldFont(ofSize: 18)

        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 12
        layout.minimumInteritemSpacing = 0
        
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CourseCollectionViewCell.self, forCellWithReuseIdentifier: CourseCollectionViewCell.cellIdentifier())
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)

    }
}

extension HorizontalItemListCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CourseCollectionViewCell.cellIdentifier(), for: indexPath) as! CourseCollectionViewCell
        
        cell.configure(itemList[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedClosure(itemList[indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 170, height: 220)
    }
    
}
