//
//  CityLauncher.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

class CityLauncher: NSObject{
    var getListByCityClosure = {(_ city: String) -> () in}

    var cityArray = [City]() {
        didSet {
            self.tableView.reloadData()
        }
    }

    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .white
        tv.separatorStyle = .none
        
        return tv
    }()
    
    let view: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let cancelButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .white
        btn.setTitle("Отмена", for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 14)
        btn.setTitleColor(.red, for: .normal)
        return btn
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.text = "Выберите город"
        label.font = UIFont.getMontserratSemiBoldFont(ofSize: 16)
        return label
    }()
    
    let blackView = UIView()
    
    
    override init() {
        super.init()
        setupViews()
    }

    func showSettings(){
        if let window = UIApplication.shared.keyWindow{
            
            window.addSubview(blackView)
            window.addSubview(view)

            
            blackView.frame = window.frame
            blackView.alpha = 1
            
            let height = window.frame.height * 0.62
            
            view.frame = CGRect(x: 24, y: window.frame.height/5.3, width: window.frame.width-48, height: height)

        }
    }
    
    private func dismissFunc(){
        self.blackView.alpha = 0
        
        if let window = UIApplication.shared.keyWindow {
            self.view.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width-32, height: self.tableView.frame.height)
            
            
        }
    }
    

    @objc func handleCancel(){
        dismissFunc()
    }
    
    
    @objc func handleDismiss(){
        dismissFunc()
    }
}

extension CityLauncher {
    private func setupViews() {
        addSubviews()
        setupConstraints()
        stylizeViews()
        setupActions()
    }
    
    private func addSubviews() {
        
        view.addSubview(cancelButton)
        view.addSubview(tableView)
        view.addSubview(label)
    }
    
    private func setupConstraints() {
        
        cancelButton.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(60)
        }
        
        label.snp.makeConstraints { (make) in
            make.top.right.equalToSuperview()
            make.left.equalTo(12)
            make.height.equalTo(60)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(label.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(cancelButton.snp.top)
        }
    }
    
    private func stylizeViews() {
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellIdentifier())

    }
    
    private func setupActions() {
        cancelButton.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))

    }
}

extension CityLauncher: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.cellIdentifier(), for: indexPath)
        cell.textLabel?.text = cityArray[indexPath.row].name
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = .getMontserratRegularFont(ofSize: 14)
        cell.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismissFunc()
        getListByCityClosure(cityArray[indexPath.row].name)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

