//
//  AllItemsViewController.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController {

    private var viewModel: ItemListViewModel = ItemListViewModel()

    private var tableView = UITableView()
    private var cityLauncher = CityLauncher()
    private var cityFilterView = CityFilterView()
    private var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.bind(to: viewModel)
        self.reloadPage()
        // Do any additional setup after loading the view.
    }
    

    private func bind(to viewModel: ItemListViewModel) {
        viewModel.error.observe(on: self) { [weak self] in
            guard  let `self` = self else { return }
            self.showAlert(type: .error, $0)}
        viewModel.loading.observe(on: self) { loading in
            if (loading) {
                LoaderView.show()
            } else {
                self.view.isHidden = false
                self.tableView.refreshControl?.endRefreshing()
                LoaderView.hide()
            }
        }
        viewModel.list.observe(on: self) { [weak self] in
            if $0.count > 0 {
                guard  let `self` = self else { return }
                self.tableView.reloadData()
            }
        }
        
        viewModel.cityList.observe(on: self) { [weak self] in
            if $0.count > 0 {
                guard  let `self` = self else { return }
                self.cityLauncher.cityArray = viewModel.cityList.value
            }
        }
    }
    
    private func reloadPage() {
        self.view.isHidden = true
        viewModel.getItemList()
    }
    
    @objc func updateList() {
        viewModel.getItemList()
    }
}

extension ItemListViewController {
    private func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
        setupAction()
    }
    
    private func addSubviews() {
        view.addSubview(cityFilterView)
        view.addSubview(tableView)
    }
    
    private func addConstraints() {
        cityFilterView.snp.makeConstraints { (make) in
            make.top.equalTo(AppConstants.statusBarHeight)
            make.left.right.equalToSuperview()
            make.height.equalTo(AppConstants.navBarHeight)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(cityFilterView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
    private func stylizeViews() {
        self.navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .backColor
        
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HorizontalItemListCell.self, forCellReuseIdentifier: HorizontalItemListCell.cellIdentifier())
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        
        refreshControl.tintColor = .lightGray
        refreshControl.addTarget(self, action: #selector(updateList), for: .valueChanged)
    }
    
    private func setupAction() {
        cityFilterView.getCityListClosure = { [weak self] in
            guard let `self` = self else { return }
            self.cityLauncher.showSettings()
        }
        
        cityLauncher.getListByCityClosure = { [weak self] city in
            guard let `self` = self else { return }
            self.cityFilterView.setButtonAttributeString(city)
            self.reloadPage()
        }
    }
}

extension ItemListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HorizontalItemListCell.cellIdentifier()) as! HorizontalItemListCell
        if viewModel.list.value.count > indexPath.row {
            cell.configureValue(title: viewModel.list.value[indexPath.row].type, itemList: viewModel.list.value[indexPath.row].value)
        }
        
        cell.selectedClosure = { [weak self] item in
            guard let `self` = self else { return }
            self.navigationController?.pushViewController(ItemDetailViewController(id: item.id), animated: true)
        }
        
        return cell
    }
    
    
}
