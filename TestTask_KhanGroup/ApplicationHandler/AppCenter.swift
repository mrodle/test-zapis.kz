//
//  AppCenter.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

class AppCenter{
    
    //MARK: - Properties
    var window: UIWindow = UIWindow()
    static let shared = AppCenter()
    
    
    //MARK: - Start functions
    func createWindow(_ window: UIWindow) -> Void {
        self.window = window
    }
    
    func start() -> Void {
        makeKeyAndVisible()
        makeRootController()
    }
        
    private func makeKeyAndVisible() -> Void {
        window.backgroundColor = .white
        window.makeKeyAndVisible()
    }
     
    private func makeRootController() -> Void {
        window.rootViewController = UINavigationController(rootViewController: ItemListViewController())
    }
}
