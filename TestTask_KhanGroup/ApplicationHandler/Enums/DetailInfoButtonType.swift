//
//  DetailInfoButtonType.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
enum DetailInfoButtonType {
    case location
    case instagram
    case phone
}
