//
//  AlertMessageType.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
enum AlertMessageType: String {
    case error = "Ошибка"
    case warning = "Внимание"
    case success = "Успешно"
    case none = ""
}
