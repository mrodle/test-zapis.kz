//
//  ItemRatingInfoType.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
enum ItemRatingInfoType {
    case check
    case rating
}
