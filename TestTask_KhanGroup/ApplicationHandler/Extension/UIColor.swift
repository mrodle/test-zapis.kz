//
//  UIColor.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let mainColor = #colorLiteral(red: 0, green: 0.004, blue: 0.988, alpha: 1)
    static let textColor = #colorLiteral(red: 0.039, green: 0.063, blue: 0.204, alpha: 1)
    static let upperColor = #colorLiteral(red: 0.106, green: 0.208, blue: 0.306, alpha: 1)
    static let selectionColor = #colorLiteral(red: 1, green: 0.875, blue: 0.22, alpha: 1)
    static let anotherBlue = #colorLiteral(red: 0.631372549, green: 0.7215686275, blue: 0.8117647059, alpha: 1)
    static let backColor = #colorLiteral(red: 252.0/255.0, green: 252.0/255.0, blue: 252.0/255.0, alpha: 1.0)
}

