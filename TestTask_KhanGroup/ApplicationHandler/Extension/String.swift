//
//  String.swift
//  TestTask_KhanGroup
//
//  Created by Eldor Makkambayev on 8/1/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
extension String {
    var serverUrlString: String {
        return "http://zp.jgroup.kz" + self
    }
    
    var url: URL {
        return URL(string: self)!
    }
}
