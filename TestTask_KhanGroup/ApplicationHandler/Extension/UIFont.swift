//
//  UIFont.swift
//  TestTask_forKhan
//
//  Created by Eldor Makkambayev on 7/31/20.
//  Copyright © 2020 Eldor Makkambayev. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static func getSFProSemiboldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Semibold", size: ofSize)!
    }
    static func getSFProBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Bold", size: ofSize)!
    }
    static func getSFProRegularFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Regular", size: ofSize)!
    }
    static func getSFProMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Medium", size: ofSize)!
    }
    static func getMontserratMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: ofSize)!
    }
    static func getMontserratSemiBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-SemiBold", size: ofSize)!
    }
    static func getMontserratRegularFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: ofSize)!
    }
    
}

