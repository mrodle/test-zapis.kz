#import <UIKit/UIKit.h>

namespace yandex { namespace maps { namespace runtime { namespace view {
class PlatformView;
}}}} // namespace yandex::maps::runtime::view

@class YRTTouchEvent;

@interface YRTView : UIView

- (yandex::maps::runtime::view::PlatformView*)getPlatformView;
- (void)setNoninteractive:(bool)is;
- (void)handleTouchEvent:(YRTTouchEvent *)touchEvent;
- (void)deinitialize;

@end

@interface YRTViewFactory : NSObject

+ (YRTView*)createViewWithFrame:(CGRect)frame vulkanEnabled:(BOOL)vulkanEnabled;
+ (YRTView*)createViewWithFrame:(CGRect)frame
                                  scale:(float)scale
                          vulkanEnabled:(BOOL)vulkanEnabled;

@end

